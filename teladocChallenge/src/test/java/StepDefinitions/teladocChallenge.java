package StepDefinitions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class teladocChallenge {

	String projectPath = System.getProperty("user.dir");
	WebDriver driver = null;

	@Given("User is on main page")
	public void user_is_on_main_page() {
		// Validate user is on add user page
		System.setProperty("webdriver.chrome.driver", projectPath+"/src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get ("http://www.way2automation.com/angularjs-protractor/webtables/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if(driver.findElement(By.xpath("/html/body/table/thead/tr[2]/td/button")) != null){
			System.out.println("User is on main page");
		}else{
			System.out.println("Main page not loaded successfully");
		}
	}
	@When("^User click on add new user button")
	public void user_click_on_add_button() {
		// Click on add user
		
		driver.findElement(By.xpath("/html/body/table/thead/tr[2]/td/button")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(driver.findElement(By.name("FirstName")) != null){
			System.out.println("User is on add new user page");
		}else{
			System.out.println("Add new user page not loaded successfully");
		}
	}
	@And("^User fills new user form (.*) and (.*) and (.*) and (.*) and (.*) and (.*) and (.*) and (.*)$")
	public void user_fills_new_user_form(String FirstName, String LastName, String Password, String UserName, String Customer, String Role, String Email, int Cell) throws InterruptedException {
		// Enter first name
		String Phone = Integer.toString(Cell);
		enter_text_in_edit_field("FirstName", FirstName);
		enter_text_in_edit_field("LastName", LastName);
		enter_text_in_edit_field("UserName", UserName);
		enter_text_in_edit_field("Password", Password);
		TimeUnit.SECONDS.sleep(1);
		select_radio_button(Customer);
		TimeUnit.SECONDS.sleep(1);
		select_drop_down("filedid", Role);
		enter_text_in_edit_field("Email", Email);
		enter_text_in_edit_field("Mobilephone", Phone);
	}
	@And("User clicks on Save buton")
	public void user_clicks_on_save_buton() {
		driver.findElement(By.xpath("/html/body/div[3]/div[3]/button[2]")).click();
	}
	@SuppressWarnings("deprecation")
	@Then("^User (.*) added successfully to table$")
	public void user_added_successfully_to_table(String FirstName) {
		int cols;
		int rows;
		boolean userAdd = false;
		String rowtext = null;
		
		WebElement baseTable = driver.findElement(By.tagName("table"));	    
	    List  col = driver.findElements(By.xpath("/html/body/table/thead/tr/th"));
        List  row = driver.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"));
        cols = col.size();
		rows = row.size();
        
        for (int i = 1; i < cols; i++) {
        	for (int j = 1; j < rows; j++) {
        		WebElement tableRow = baseTable.findElement(By.xpath("/html/body/table/tbody/tr["+j+"]/td["+i+"]"));
        		rowtext = tableRow.getText();
        	    if (rowtext.equals(FirstName)) {
        	    	userAdd = true;
  				  break;
  				} 
        	}
        	if (userAdd == true) {
        		break;
        	}
        }
        if (userAdd != true) {
        	Assert.assertEquals("User addition failed for user FirstName - "+FirstName, FirstName, rowtext);
        }

	}
	@When("^User deletes(.*)$")
	public void delete_user(String FirstName) {
		int cols;
		int rows;
		boolean userDelete = false;
		String rowtext = null;
		
		WebElement baseTable = driver.findElement(By.tagName("table"));	    
	    List  col = driver.findElements(By.xpath("/html/body/table/thead/tr/th"));
        List  row = driver.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"));
        cols = col.size();
		rows = row.size();
        
        for (int i = 1; i < cols; i++) {
        	for (int j = 1; j < rows; j++) {
        		WebElement tableRow = baseTable.findElement(By.xpath("/html/body/table/tbody/tr["+j+"]/td["+i+"]"));
        		rowtext = tableRow.getText();
        	    if (rowtext.equals(FirstName)) {
        	    	userDelete = true;
        	    	driver.findElement(By.xpath("/html/body/table/tbody/tr["+j+"]/td[11]/button/i")).click();
        	    	driver.findElement(By.xpath("/html/body/div[3]/div[3]/button[2]")).click();
  				  break;
  				} 
        	}
        	if (userDelete == true) {
        		break;
        	}
        }
	}
	@Then("^User (.*) deleted successfully$")
	public void user_deleted_successfully(String FirstName) {
		int cols;
		int rows;
		boolean userDelete = true;
		String rowtext = null;
		
		WebElement baseTable = driver.findElement(By.tagName("table"));	    
	    List  col = driver.findElements(By.xpath("/html/body/table/thead/tr/th"));
        List  row = driver.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"));
        cols = col.size();
		rows = row.size();
		
		for (int i = 1; i < cols; i++) {
        	for (int j = 1; j < rows; j++) {
        		WebElement tableRow = baseTable.findElement(By.xpath("/html/body/table/tbody/tr["+j+"]/td["+i+"]"));
        		rowtext = tableRow.getText();
        	    if (rowtext.equals(FirstName)) {
        	    	userDelete = false;
  				  break;
  				} 
        	}
        	if (userDelete == false) {
        		break;
        	}
        }
        
        if (userDelete == true) {
        	Assert.assertEquals("User deletion failed for user FirstName - "+FirstName, FirstName, rowtext);
        }
	}
	
	@Then("User closes browser")
	public void browser_close() {
		driver.close();
		driver.quit();
	}
	
	public void enter_text_in_edit_field(String field, String text) {
		driver.findElement(By.name(field)).sendKeys(text);
	}
	
	public void select_radio_button(String text) {
		
		String btn = null;
		switch(text) {
		  case "Company AAA":
		    btn = "/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[1]";
		    break;
		  case "Company BBB":
			btn = "/html/body/div[3]/div[2]/form/table/tbody/tr[5]/td[2]/label[2]";
		    break;
		}
		driver.findElement(By.xpath(btn)).click();
	}
	
	public void select_drop_down(String field, String text) {
		Select RoleId=new Select(driver.findElement(By.name("RoleId")));
		RoleId.selectByVisibleText(text);
	}

}
